import 'package:flutter/material.dart';

 InputDecoration textFieldDecoration(String hintText){
   return InputDecoration(
     counterText: "",
     hintText: hintText,
     fillColor: Colors.grey[850],
     hintStyle: const TextStyle(color: Colors.white),
     contentPadding: const EdgeInsets.all(10),
     focusedBorder: OutlineInputBorder(
         borderRadius: BorderRadius.circular(12),
         borderSide: const BorderSide(color: Colors.transparent)),
     border: InputBorder.none,
     filled: true,
   );
 }

 BoxDecoration innerDecoration(){
   return BoxDecoration(
       color: Colors.grey[850],
       borderRadius: const BorderRadius.all(Radius.circular(100)),
       // border: Border.all(width: 10,color: Colors.grey[900]!),
       boxShadow: [
         BoxShadow(
             color: Colors.grey.withOpacity(0.3),
             offset: Offset(4.0, 4.0),
             blurRadius: 3.0,
             spreadRadius: 1.0),
         const BoxShadow(
             color: Colors.black,
             offset: Offset(-4.0, -4.0),
             blurRadius: 3.0,
             spreadRadius: 1.0),
       ]);
 }

BoxDecoration squareDecoration(double radius){
  return BoxDecoration(
      color: Colors.grey[850],
      borderRadius:  BorderRadius.all(Radius.circular(radius)),
      // border: Border.all(width: 10,color: Colors.grey[900]!),
      boxShadow: [
        BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            offset: Offset(4.0, 4.0),
            blurRadius: 3.0,
            spreadRadius: 1.0),
        const BoxShadow(
            color: Colors.black,
            offset: Offset(-4.0, -4.0),
            blurRadius: 3.0,
            spreadRadius: 1.0),
      ]);
}
