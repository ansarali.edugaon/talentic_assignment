import 'package:flutter/material.dart';

class AppLogoComponent extends StatelessWidget {
  const AppLogoComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      height: 150,
      child: const Center(child: Text("Talentic",style: TextStyle(color: Colors.orange,fontSize: 24,fontWeight: FontWeight.bold),)),
      decoration: BoxDecoration(
          color:  Colors.grey[850],
          borderRadius: const BorderRadius.all(Radius.circular(100)),
          border: Border.all(width: 10,color: Colors.grey[900]!),
          boxShadow: [
            const BoxShadow(
                color:  Colors.black,
                offset: Offset(4.0, 4.0),
                blurRadius: 15.0,
                spreadRadius: 1.0),
            BoxShadow(
                color: Colors.grey[700]!,
                offset: const Offset(-4.0, -4.0),
                blurRadius: 15.0,
                spreadRadius: 1.0),
          ]),
    );
  }
}
