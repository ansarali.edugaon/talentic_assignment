import 'package:flutter/material.dart';
import 'package:talentic_assignment/components/AppLogoComponent.dart';
import 'package:talentic_assignment/components/DecorationComponent.dart';
import 'package:talentic_assignment/screens/SignUpScreen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  final _loginKey = GlobalKey<FormState>();
  final usernameController = TextEditingController();
  final passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[850],
        body: Stack(
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: SingleChildScrollView(
                child: Form(
                  key: _loginKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: 40,),
                      AppLogoComponent(),
                      SizedBox(height: 40,),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 30,vertical: 10),
                        padding: EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width,
                        height: 50,
                        child: TextFormField(
                          controller: usernameController,
                          validator: (user){
                            if(user!.isEmpty){
                              return "This field is required";
                            }else{
                              return null;
                            }
                          },
                          style: const TextStyle(color: Colors.white),
                          decoration: textFieldDecoration("Username"),
                        ),
                        decoration: innerDecoration(),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 30,vertical: 10),
                        padding: const EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width,
                        height: 50,
                        child: TextFormField(
                          controller: passController,
                          validator: (user){
                            if(user!.isEmpty){
                              return "This field is required";
                            }else{
                              return null;
                            }
                          },
                          keyboardType: TextInputType.text,
                          style: const TextStyle(color: Colors.white),
                          decoration: textFieldDecoration("Password"),
                        ),
                        decoration: innerDecoration(),
                      ),
                      const SizedBox(
                        height: 50,
                      ),
                      RaisedButton(
                        onPressed: () {
                          if(_loginKey.currentState!.validate()){
                            showDialog(context: context, builder: (context)=> accountDialog());
                          }

                        },
                        textColor: Colors.white,
                        padding: const EdgeInsets.all(0.0),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80.0)),
                        child: Container(
                          decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                colors: <Color>[
                                  Color(0xFFDD86E3),
                                  Color(0xFF8717CD),
                                ],
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(60.0))),
                          padding:
                              const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                          child: const Text('LogIn', style: TextStyle(fontSize: 20)),
                        ),
                      ),
                      SizedBox(height: 100,)
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
                bottom: 30,
                left: 0,
                right: 0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children:  [
                    const Text(
                      "New To Talentic?",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                    const SizedBox(width: 10,),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => SignUpScreen()));
                      },
                      child: const Text("Create An Account",
                          style: TextStyle(
                              color: Colors.blue,
                              fontWeight: FontWeight.bold,
                              fontSize: 16)),
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }
  Widget accountDialog(){
    return AlertDialog(
      title: Text("Don't have an account"),
      content: Text("You do not have an account here. Please create your account first."),
      actions: [
        ElevatedButton(onPressed: (){
          Navigator.pop(context);
        }, child: Text("Ok"))
      ],
    );
  }

}
