import 'package:flutter/material.dart';
import 'package:talentic_assignment/screens/LoginScreen.dart';

class SuccessOtpScreen extends StatefulWidget {
  const SuccessOtpScreen({Key? key}) : super(key: key);

  @override
  _SuccessOtpScreenState createState() => _SuccessOtpScreenState();
}

class _SuccessOtpScreenState extends State<SuccessOtpScreen> {

  @override
  void initState() {
    super.initState();
    startLoginScreen();
  }

  startLoginScreen(){
    Future.delayed(Duration(seconds: 2),(){
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) =>LoginScreen()), (route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[850],
      body: Center(
        child: Container(
          height: 180,
          width: 180,
          decoration: BoxDecoration(
              color: Colors.grey[850],
              borderRadius: const BorderRadius.all(Radius.circular(100)),
              border: Border.all(width: 1,color: Colors.white),
              boxShadow: [
                BoxShadow(
                    color: Colors.red.withOpacity(0.3),
                    offset: Offset(4.0, 4.0),
                    blurRadius: 3.0,
                    spreadRadius: 1.0),
                const BoxShadow(
                    color: Colors.red,
                    offset: Offset(-2.0, -2.0),
                    blurRadius: 3.0,
                    spreadRadius: 1.0),
              ]),
          child: Column(
            children: const [
              Icon(Icons.check,color: Colors.white,size: 100,),
              Text("OTP Verify Successfully",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),)
            ],
          ),
        ),
      ),
    );
  }
}
