import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:talentic_assignment/components/AppLogoComponent.dart';
import 'package:talentic_assignment/components/DecorationComponent.dart';
import 'package:talentic_assignment/screens/OtpValidationScreen.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {

  final signupKey = GlobalKey<FormState>();

  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passController = TextEditingController();
  final _mobileController = TextEditingController();

  String? validatePassword(String value) {
    RegExp regex =
    RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$');
    if (value.isEmpty) {
      return 'Please a strong password';
    } else {
      if (!regex.hasMatch(value)) {
        return 'Please a strong password';
      } else {
        return null;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[850],
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Form(
              key: signupKey,
              child: Column(
                children: [
                  SizedBox(height: 40,),
                  AppLogoComponent(),
                  SizedBox(height: 40,),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 30,vertical: 10),
                    padding: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    child: TextFormField(
                      controller: _nameController,
                      validator: (name) {
                        if(name!.isEmpty){
                          return "This is a required field";
                        }else{
                          return null;
                        }

                      },
                      style: TextStyle(color: Colors.white),
                      decoration: textFieldDecoration("Full Name"),
                    ),
                    decoration: innerDecoration(),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 30,vertical: 10),
                    padding: const EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    child: TextFormField(
                      autofillHints: const [AutofillHints.email],
                      validator: (email) => email != null && !EmailValidator.validate(email)?"Enter a valid email":null,
                      controller: _emailController,
                      keyboardType: TextInputType.emailAddress,
                      style: const TextStyle(color: Colors.white),
                      decoration: textFieldDecoration("Email"),
                    ),
                    decoration: innerDecoration(),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 30,vertical: 10),
                    padding: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    child: TextFormField(
                      validator: (pass) => validatePassword(pass!),
                      controller: _passController,
                      style: TextStyle(color: Colors.white),
                      decoration: textFieldDecoration("Password"),
                    ),
                    decoration: innerDecoration(),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 30,vertical: 10),
                    padding: const EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    child: TextFormField(
                      validator: (name) {
                        if(name!.length <9){
                          return "Enter valid number";
                        }else{
                          return null;
                        }
                      },
                      controller: _mobileController,
                      maxLength: 10,
                      keyboardType: TextInputType.number,
                      style: const TextStyle(color: Colors.white),
                      decoration: textFieldDecoration("Mobile Number"),
                    ),
                    decoration: innerDecoration(),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  RaisedButton(
                    onPressed: () {
                      if(signupKey.currentState!.validate()){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => OtpValidationScreen(number: _mobileController.text,)));
                      }
                    },
                    textColor: Colors.white,
                    padding: const EdgeInsets.all(0.0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(80.0)),
                    child: Container(
                      decoration: const BoxDecoration(
                          gradient: LinearGradient(
                            colors: <Color>[
                              Color(0xFF85B5E8),
                              Color(0xFF0E4D90),
                            ],
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(60.0))),
                      padding:
                      const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                      child: const Text('Signup', style: TextStyle(fontSize: 20)),
                    ),
                  ),
                  SizedBox(height: 40,),
                  Text("Or",style: TextStyle(fontSize: 20,color: Colors.white,fontWeight: FontWeight.bold)),
                  SizedBox(height: 20,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        height: 50,
                        width: 50,
                        decoration: innerDecoration(),
                        child: Image.asset("images/g_logo.png"),
                      ),
                      SizedBox(width: 15,),
                      Container(
                        padding: EdgeInsets.all(10),
                        height: 50,
                        width: 50,
                        decoration: innerDecoration(),
                        child: Image.asset("images/t_logo.png"),
                      ),
                      SizedBox(width: 15,),
                      Container(
                        padding: EdgeInsets.all(10),
                        height: 50,
                        width: 50,
                        decoration: innerDecoration(),
                        child: Image.asset("images/f_logo.png"),
                      ),
                      SizedBox(width: 15,),
                      Container(
                        padding: EdgeInsets.all(10),
                        height: 50,
                        width: 50,
                        decoration: innerDecoration(),
                        child: Image.asset("images/i_logo.png",height: 40,width: 40,),
                      )
                    ],
                  ),
                  SizedBox(height: 50,),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
